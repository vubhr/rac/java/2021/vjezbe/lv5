package hr.vub;

public class Car extends Vehicle implements Wheelbase, Pickable{


    @Override
    public void Drive() {
        System.out.println("Drive car!");
    }

    @Override
    public void getVehicleWheelbase() {
        System.out.println("Car have 4 wheels!");
    }
}
