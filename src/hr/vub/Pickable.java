package hr.vub;

public interface Pickable {
    default void PickVehicle(){
        System.out.println("Regular crane is used for picking vehicle!");
    }
}
