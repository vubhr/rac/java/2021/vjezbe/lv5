package hr.vub;

public class Truck extends Vehicle implements Wheelbase, Pickable {
    @Override
    public void Drive() {
        System.out.println("Drive truck!");
    }

    @Override
    public void getVehicleWheelbase() {
        System.out.println("Truck have 8 wheels!");
    }
}
