package hr.vub;

public abstract class Vehicle {

    public abstract void Drive();
}
