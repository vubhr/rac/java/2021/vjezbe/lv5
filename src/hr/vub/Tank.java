package hr.vub;

public class Tank extends Vehicle implements Pickable {

    @Override
    public void Drive() {
        System.out.println("Drive tank!");
    }

    @Override
    public void PickVehicle() {
        System.out.println("Tank is picked by big heavy duty crane!");
    }
}
