package hr.vub;

public class Main {

    public static void main(String[] args) {
        Vehicle[] vehicles = new Vehicle[5];
        vehicles[0] = new Car();
        vehicles[1] = new MotorBike();
        vehicles[2] = new Truck();
        vehicles[3] = new Plane();
        vehicles[4] = new Tank();

        for (Vehicle vehicle: vehicles) {

            //Check wheel base
            if(vehicle instanceof Wheelbase){
                Wheelbase wb = (Wheelbase) vehicle;
                wb.getVehicleWheelbase();
            }
            //Check picking solution
            if(vehicle instanceof Pickable){
                Pickable pb = (Pickable) vehicle;
                pb.PickVehicle();
            }

            //Drive each vehicle
            vehicle.Drive();
        }

    }
}
