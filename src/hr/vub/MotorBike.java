package hr.vub;

public class MotorBike extends Vehicle implements Wheelbase, Pickable {


    @Override
    public void Drive() {
        System.out.println("Drive motor bike!");
    }

    @Override
    public void getVehicleWheelbase() {
        System.out.println("Motor bike have 2 wheels!");
    }
}
