package hr.vub;

public class Plane extends Vehicle implements Wheelbase, Pickable {


    @Override
    public void Drive() {
       System.out.println("Drive plane!");
    }

    @Override
    public void getVehicleWheelbase() {
        System.out.println("Plane have 8 wheels!");
    }

    @Override
    public void PickVehicle() {
        System.out.println("Plane is picked by plane carrier!");
    }
}
